class BaseCar
  attr_reader :current_speed, :brand, :max_speed

  def initialize
    @current_speed = 0
    @brand = "Unknown"
    @max_speed = 0
  end

  def accelerate
    @current_speed+=1
  end

  def drive
    puts "Ignition started"
    ## Execution 1: Single line code
    #accelerate while @current_speed < @max_speed

    ##Execution 2: Prints current_speed for every increment in acceleration
    while @current_speed < @max_speed
      accelerate
      puts "Current Speed: #{@current_speed}" #to show that accelerate is increasing by 1
    end
    puts "Camaro reached max speed of #{@current_speed}"
  end

end

class CamaroCar < BaseCar
  def initialize
    super
    @brand = "Chevy"
    @max_speed = 200
  end
end

camaro = CamaroCar.new
camaro.drive
